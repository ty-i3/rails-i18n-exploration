How To
----
To run this:
$ rbenv exec rails server

... and then open the following URL in your browser:
http://localhost:3000/salut/open

Change the locale using a URL parameter:
http://localhost:3000/salut/open?locale=ja


Reference Material
----
Create a new skeleton Ruby on Rails project:
https://gorails.com/setup/osx/10.11-el-capitan

Ruby on Rails and I18n:
https://guides.rubyonrails.org/i18n.html

I18n tasks:
https://github.com/glebm/i18n-tasks

I18n live editor:
https://github.com/BrucePerens/i18n-edit
